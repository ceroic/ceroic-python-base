# Python Base

Simple base image that uses the official Python image as a base and includes 
NodeJS in order to simplify building application images.

## Available Tags

* 3.5 - Based off of python:3.5.2
* 2.7 - Based off of python:2.7.12

## Usage

```
FROM ceroic/python-base:3.5
```

